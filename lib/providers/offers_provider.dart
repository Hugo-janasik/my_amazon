import 'dart:async';
import 'package:flutter/material.dart';
import 'package:dio/dio.dart';
import 'package:mywebsite/model/offers_model.dart';
import 'package:mywebsite/services/requestor.dart';
import 'package:flutter/foundation.dart';

class OffersProvider extends ChangeNotifier {
  OffersProvider();

  Future<List<Offers>> offersFromApi(List<String> items) async {
    if (items.isEmpty) {
      return [];
    }
    final Response<dynamic> res = await Requestor.makeRequest(
      Method.verbGet,
      'books/${items.toString().replaceAll('[', '').replaceAll(']', '')}/commercialOffers',
    );

     final List<Offers> offers = [];

    for (Map<String, dynamic> book in res.data['offers']) {
      offers.add(Offers.fromMap(book));
    }
    return offers;
  }
}