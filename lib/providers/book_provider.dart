import 'dart:async';

import 'package:flutter/material.dart';
import 'package:dio/dio.dart';
import 'package:mywebsite/model/book_model.dart';
import 'package:mywebsite/services/requestor.dart';
import 'package:flutter/foundation.dart';

class BookProvider extends ChangeNotifier {
  BookProvider();

   Future<List<Book>> bookFromAPi() async {
    final Response<dynamic> res = await Requestor.makeRequest(
        Method.verbGet, 'books',
    );

    final List<Book> books = [];

    for (Map<String, dynamic> book in res.data ) {
      books.add(Book.fromMap(book));
    }
    return books;

  }
}