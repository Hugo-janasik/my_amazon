import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_dotenv/flutter_dotenv.dart';
import 'package:mywebsite/app.dart';
import 'package:mywebsite/providers/book_provider.dart';
import 'package:mywebsite/providers/offers_provider.dart';
import 'package:mywebsite/services/requestor.dart';
import 'package:mywebsite/utils/globals.dart';
import 'package:provider/provider.dart';
import 'package:provider/single_child_widget.dart';
import 'package:firebase_core/firebase_core.dart';


Future<void> mainCommon(String dotenvPath) async {
  WidgetsFlutterBinding.ensureInitialized();
  await Firebase.initializeApp(
    options: const FirebaseOptions(
      apiKey: "XXX",
      appId: "XXX",
      messagingSenderId: "XXX",
      projectId: "XXX",
    ),
  );

  final String env = await rootBundle.loadString(dotenvPath);
  if (env.isEmpty) {
    return;
  }
  await dotenv.load(fileName: dotenvPath);

  GlobalConfig.showPrettylogger = false;
  GlobalConfig.isPreviewScreenEnabled = false;

  Requestor.init();

  runApp(createAppProvider(false));

}

MultiProvider createAppProvider(bool withDevicePreview) =>
  MultiProvider(providers: <SingleChildWidget>[
    ChangeNotifierProvider<BookProvider>(
      create: (_) => BookProvider(),
    ),
    ChangeNotifierProvider<OffersProvider>(
      create: (_) => OffersProvider(),
    )
  ],child: const MyApp(),);


