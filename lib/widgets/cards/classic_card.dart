import 'package:flutter/material.dart';
import 'package:mywebsite/widgets/rating_stars.dart/rating_stars.dart';

class MyCard extends StatelessWidget {
  final String title;
  Widget? subtitle;
  Widget? leading;
  Widget? trailing;


  MyCard({Key? key, required this.title, this.subtitle, this.leading, this.trailing}) : super(key: key);

  @override
  Widget build(BuildContext context) =>  Padding(
    padding: const EdgeInsets.all(20),
    child: Card(
      margin: const EdgeInsets.symmetric(horizontal: 150),
      child: ListTile(
        trailing: trailing,
        title: Text(title),
        leading: leading ?? const RatingStars(),
        subtitle: subtitle,
      ),
    ),
  );
}