import 'package:flutter/material.dart';

class MyDivider extends StatelessWidget {
  const MyDivider({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) => const Divider(
    thickness: 5,
    indent: 270,
    endIndent: 270,
    color: Color.fromARGB(255, 193, 193, 193),
    height: 20,
  );
}