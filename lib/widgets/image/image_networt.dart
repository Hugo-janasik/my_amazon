import 'package:flutter/widgets.dart';

class ImageNetwork extends StatelessWidget {
  final String urlCover;
  final Color? color;

  const ImageNetwork({Key? key, required this.urlCover, this.color}) : super(key: key);

  @override
  Widget build(BuildContext context) =>  Container(
    color: color ?? const Color.fromARGB(102, 201, 199, 199),
    child: Image.network(urlCover),
    width: 200,
    height: 200,
  );
}