import 'package:flutter/material.dart';
import 'package:mywebsite/utils/responsive.dart';

class Search<T> extends SearchDelegate<T> {
  @override
  ThemeData appBarTheme(BuildContext context) {
    final ThemeData theme = Theme.of(context);
    return theme;
  }

  @override
  List<Widget> buildActions(BuildContext context) {
    return <Widget>[
      IconButton(icon: const Icon(Icons.close), onPressed: () => query = '')
    ];
  }

  @override
  Widget buildLeading(BuildContext context) {
    return IconButton(
      icon: const Icon(Icons.arrow_back),
      onPressed: () => Navigator.pop(context),
    );
  }

  @override
  Widget buildResults(BuildContext context) {
    return Column(
      children: <Widget>[
        FutureBuilder<List<String>>(
          future: getGlobalMedicInfo() as Future<List<String>>?,
          builder:
              (BuildContext context, AsyncSnapshot<List<String>> snapshot) {
            if (!snapshot.hasData) {
              return Column(
                crossAxisAlignment: CrossAxisAlignment.center,
                mainAxisAlignment: MainAxisAlignment.center,
                children: const <Widget>[
                  Center(
                    child: CircularProgressIndicator(),
                  ),
                ],
              );
            } else if (snapshot.data!.isEmpty) {
              return Column(
                children: const <Widget>[
                  Text('Aucun Résultats'),
                ],
              );
            } else {
              final List<String> results = snapshot.data!;
              return ListView.builder(
                  itemCount: results.length,
                  itemBuilder: (BuildContext context, int index) {
                    final String result = results[index];
                    return ListTile(
                      title: Text(result),
                    );
                  });
            }
          },
        )
      ],
    );
  }

  @override
  Widget buildSuggestions(BuildContext context) {
    return Column();
  }

  Future<dynamic>? getGlobalMedicInfo() {
    return null;
  }
}

class SearchBar extends TextFormField {
  SearchBar(
    BuildContext context, {
    required ValueChanged<String> onChanged,
    String? hintText,
    String initialValue = '',
    Key? key,
  }) : super(
          key: key,
          onChanged: onChanged,
          initialValue: initialValue,
          autocorrect: false,
          cursorColor: Colors.blue,
          decoration: InputDecoration(
            filled: false,
            suffixIcon: const Icon(Icons.search),
            hintText: hintText,
            hintMaxLines: 1,
            hintStyle: TextStyle(
                fontWeight: FontWeight.w300,
                /*color: Theme.of(context).brightness == Brightness.light
                    ? ThemeData.dark()
                    : ConfigTheme.light().appBarTheme.backgroundColor,*/
                fontSize: getFontSizeSubTitleResponsive(context)),
            enabledBorder: const UnderlineInputBorder(
              borderSide: BorderSide(
                color: Colors.grey,
              ),
            ),
            focusedBorder: const UnderlineInputBorder(
              borderSide: BorderSide(
                color: Colors.green,
              ),
            ),
          ),
          maxLines: 1,
        );
}
