import 'package:flutter/material.dart';
import 'package:flutter_rating_bar/flutter_rating_bar.dart';
import 'dart:math';

class RatingStars extends StatefulWidget {

  const RatingStars({Key? key}) : super(key: key);

  @override
  State<RatingStars> createState() => _RatingStarsState();
}

class _RatingStarsState extends State<RatingStars> {
    Random random = Random();

  @override
  Widget build(BuildContext context) {
    return RatingBar(
      itemSize: 25,
      initialRating: random.nextInt(5) as double,
      direction: Axis.horizontal,
      itemCount: 5,
      onRatingUpdate: (double value) {},
      ratingWidget: RatingWidget(
        full: const Icon(Icons.star, color: Colors.orange),
        half: const Icon(
          Icons.star_half,
          color: Colors.orange,
        ),
        empty: const Icon(
          Icons.star_outline,
          color: Colors.orange,
        )
      ),
    );
  }
}