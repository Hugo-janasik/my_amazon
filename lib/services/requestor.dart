import 'package:bot_toast/bot_toast.dart';
import 'package:dio/dio.dart';
import 'package:flutter/foundation.dart';
import 'package:flutter_dotenv/flutter_dotenv.dart';
import 'package:mywebsite/utils/exception.dart';
import 'package:mywebsite/utils/globals.dart';
import 'package:pretty_dio_logger/pretty_dio_logger.dart';

enum Method { verbGet, verbPost, verbDelete }

class Requestor {
  Requestor.init() {
    if (kDebugMode && GlobalConfig.showPrettylogger) {
      _dio.interceptors.add(
        PrettyDioLogger(
            requestHeader: true,
            requestBody: true,
            responseBody: true,
            responseHeader: true,
            error: true,
            compact: true,
            maxWidth: 90),
      );
    }
  }

  static Dio get dio => _dio;
  static String get url => _url;

  static final Dio _dio = Dio();
  static final String _url = dotenv.env['URL_API']!;

  static const int _queryTimeout = (kReleaseMode ? 60 : 3) * 1000;

  static Future<Response<dynamic>> makeRequest(
    Method method,
    String uri, {
    Map<String, dynamic>? qParams,
    Map<String, dynamic>? body,
  }) async {
    try {
      switch (method) {
        case Method.verbGet:
          final Response<dynamic> result = await _dio.get<dynamic>(
            '$_url/$uri',
            queryParameters: qParams,
            options: Options(
              sendTimeout: _queryTimeout,
              receiveTimeout: 60*1000,
              headers: <String, dynamic>{
                'Content-Type': 'application/json',
              },
            ),
          );
          if (result.statusCode != 200 && result.statusCode != 400) {
            throw result;
          }
          return result;
        case Method.verbPost:
          final Response<dynamic> result = await _dio.post<dynamic>(
            '$_url/$uri',
            queryParameters: qParams,
            data: body,
            options: Options(
              validateStatus: (int? status) => status == 200 || status == 400,
              sendTimeout: _queryTimeout,
              receiveTimeout: 60*1000,
              headers: <String, dynamic>{
                'Content-Type': 'application/json',
              },
            ),
          );
          if (result.statusCode != 200 && result.statusCode != 400) {
            throw result;
          }
          return result;
        case Method.verbDelete:
          return _dio.delete<dynamic>(
            '$_url/$uri',
            queryParameters: qParams,
            data: body,
            options: Options(
              sendTimeout: _queryTimeout,
              receiveTimeout: 60*1000,
              headers: <String, dynamic>{
                'Content-Type': 'application/json',
              },
            ),
          );
        default:
          throw UnimplementedError();
      }
    } on DioError catch (e) {
      if (e.type == DioErrorType.receiveTimeout) {
        throw UnimplementedError();
      }
      if (e.type != DioErrorType.other) {
        BotToast.closeAllLoading();
        throw ErrorException.fromJson(e.response?.data['error']);
      }
      throw UnimplementedError();
    }
  }
}