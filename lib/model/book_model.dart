import 'package:flutter/widgets.dart';

import 'package:scoped_model/scoped_model.dart';


class Book extends Model with WidgetsBindingObserver {
  String isbn;
  String title;
  int price;
  String urlCover;
  List<String> synopsis;

  Book({
    required this.isbn,
    required this.title,
    required this.price,
    required this.urlCover,
    required this.synopsis,
  });

  factory Book.empty() => Book(
    isbn: '',
    title: '',
    price: 0,
    urlCover: '',
    synopsis: [''],
  );

  Book copyWith({
    String? isbn,
    String? title,
    int? price,
    String? urlCover,
    List<String>? synopsis,
  }) {
    this.isbn= isbn ?? this.isbn;
    this.title = title ?? this.title;
    this.price = price ?? this.price;
    this.urlCover = urlCover ?? this.urlCover;
    this.synopsis = synopsis ?? this.synopsis;

    return this;
  }

  factory Book.fromMap(Map<String, dynamic> map) => Book(
    isbn: map['isbn'],
    title: map['title'],
    price: map['price'],
    urlCover: map['cover'],
    synopsis: List.from(map['synopsis']),
  );

}