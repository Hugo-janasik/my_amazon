import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';

import 'package:scoped_model/scoped_model.dart';

class Commentary extends Model with WidgetsBindingObserver {
  String name = '';
  String commentary = '';

  Commentary({
    required this.name,
    required this.commentary,
  });

  Commentary.fromJson(Map<String, dynamic> json) {
    name = json['name'] ?? '';
    commentary = json['commentary'] ?? '';
  }
}