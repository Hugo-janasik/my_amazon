import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';

import 'package:scoped_model/scoped_model.dart';

enum TypeOffers {
  NULL,
  PERCENTAGE,
  MINUS,
  SLICE,
}

class Offers extends Model with WidgetsBindingObserver {
  TypeOffers type = TypeOffers.NULL;
  int value = 0;
  int? sliceValue;

  Offers({
    required this.type,
    required this.value,
    this.sliceValue,
  });

  static TypeOffers checkEnumString(String type) {
    switch (type) {
      case "percentage":
        return TypeOffers.PERCENTAGE;
      case "minus":
        return TypeOffers.MINUS;
      case "slice":
        return TypeOffers.SLICE;
      default:
        return TypeOffers.NULL;
    }
  }

  Offers.fromJson(Map<String, dynamic> json) {
    type = checkEnumString(json['type']);
    value = json['value'] ?? 0;
    sliceValue = json['sliceValue'] ?? '';
  }

  factory Offers.fromMap(Map<String, dynamic> map) => Offers(
    type: checkEnumString(map['type']),
    value: map['value'],
    sliceValue: map['sliceValue'],
  );

}