import 'package:flutter/material.dart';
import 'package:mywebsite/model/book_model.dart';
import 'package:mywebsite/screen/books_screen.dart';
import 'package:scoped_model/scoped_model.dart';
import 'package:bot_toast/bot_toast.dart';

class MyApp extends StatefulWidget {
  const MyApp({Key? key}) : super(key: key);

  @override
  _MyAppState createState() => _MyAppState();
}

class _MyAppState extends State<MyApp> {

  @override
  Widget build(BuildContext context) {
    return ScopedModel<Book>(
      model: Book.empty(),
      child: MaterialApp(
        debugShowCheckedModeBanner: false,
        locale: const Locale('fr', ''),
        builder: BotToastInit(),
        theme: ThemeData.light(),
        darkTheme: ThemeData.light(),
        navigatorObservers: <NavigatorObserver>[
          BotToastNavigatorObserver(),
        ],
        //routes: Routes.routes,
        home: const BookScreen(),
      ),
    );
  }
}