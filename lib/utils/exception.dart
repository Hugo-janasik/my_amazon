import 'package:mywebsite/utils/utils.dart';

class InternalServerException implements ErrorException {
  InternalServerException(this.code, this.exception, this.message);

  String message;
  String exception;
  int code;
}

class Unauthorized implements ErrorException {
  Unauthorized(this.code, this.exception, this.message);

  String message;
  String exception;
  int code;
}

class Forbidden implements ErrorException {
  Forbidden(this.code, this.exception, this.message);

  String message;
  String exception;
  int code;
}

abstract class ErrorException implements Exception {
  factory ErrorException.fromJson(Map<String, dynamic> json) {
    switch (Utils.stringToEnum(json['exception'], ExceptionEnum.values)) {
      case ExceptionEnum.InternalServerException:
        return Unauthorized(json['code'], json['exception'], json['message']);
      case ExceptionEnum.Unauthorized:
        return Unauthorized(json['code'], json['exception'], json['message']);
      case ExceptionEnum.Forbidden:
        return Forbidden(json['code'], json['exception'], json['message']);
      default:
        return InternalServerException(json['code'], json['exception'], json['message']);
    }
  }
}

enum ExceptionEnum {
  InternalServerException,
  Unauthorized,
  Forbidden
}
