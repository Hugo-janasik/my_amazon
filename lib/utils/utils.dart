import 'package:collection/collection.dart' show IterableExtension;

class Utils {
  static T? stringToEnum<T>(String? str, Iterable<T> values) => values.firstWhereOrNull(
        (T value) => value.toString().split('.')[1] == str,
      );
}