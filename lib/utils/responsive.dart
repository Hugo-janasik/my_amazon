import 'package:flutter/widgets.dart';
import 'package:responsive_builder/responsive_builder.dart';

double getIconeSizeResponsive(BuildContext context) {
  return getValueForScreenType<double>(
    context: context,
    mobile: getValueForRefinedSize<double>(context: context, normal: 22, large: 25, extraLarge: 40),
    tablet: 50,
  );
}

double getFontSizeTextResponsive(BuildContext context) {
  return getValueForScreenType<double>(
    context: context,
    mobile: getValueForRefinedSize<double>(context: context, normal: 10, large: 12, extraLarge: 16),
    tablet: 24,
  );
}

double getFontSizeSubTitleResponsive(BuildContext context) {
  return getValueForScreenType<double>(
    context: context,
    mobile: getValueForRefinedSize<double>(context: context, normal: 15, large: 16, extraLarge: 22),
    tablet: 26,
  );
}

double getFontSizeTitleResponsive(BuildContext context) {
  return getValueForScreenType<double>(
    context: context,
    mobile: getValueForRefinedSize<double>(context: context, normal: 16, large: 20, extraLarge: 26),
    tablet: 38,
  );
}

double getCardSizeResponsive(BuildContext context) {
  return getValueForScreenType<double>(
    context: context,
    mobile: getValueForRefinedSize<double>(context: context, normal: 120, large: 150, extraLarge: 170),
    tablet: 200,
  );
}

double getCupertinoSizeResponsive(BuildContext context) {
  return getValueForScreenType<double>(
    context: context,
    mobile: getValueForRefinedSize<double>(context: context, normal: 0.8, large: 1, extraLarge: 1.2),
    tablet: 1.4,
  );
}

double getMultipliantResp(BuildContext context) {
  return getValueForScreenType<double>(
    context: context,
    mobile: getValueForRefinedSize<double>(context: context, normal: 0.7, large: 1, extraLarge: 1),
    tablet: 1.8,
  );
}

double getColorPickerSize(BuildContext context) {
  return getValueForScreenType<double>(
    context: context,
    mobile: getValueForRefinedSize<double>(context: context, normal: 25, large: 30, extraLarge: 30),
    tablet: 50,
  );
}
