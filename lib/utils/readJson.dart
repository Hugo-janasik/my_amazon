import 'package:flutter/services.dart';
import 'dart:convert';

import 'package:mywebsite/model/commentary_model.dart';

Future<List<Commentary>> readJsonDataCommentary() async {
    final String jsondata = await rootBundle.loadString('json_commentary/commentary.json');
    final list = jsonDecode(jsondata);
    final List<Commentary> commentaries = [];

    for (var index in list['commentaries']) {
      commentaries.add(Commentary.fromJson(index));
    }
    return commentaries;


}