import 'package:flutter/material.dart';

class Screen {
  static double fullWidthScreen(BuildContext context) {
    return MediaQuery.of(context).size.width;
  }

  static double fullHeigthScreen(BuildContext context) {
    return MediaQuery.of(context).size.height;
  }

  static double bottomPadding(BuildContext context) {
    return MediaQuery.of(context).padding.bottom;
  }

  static double topPadding(BuildContext context) {
    return MediaQuery.of(context).padding.top;
  }

  static bool isSmallScreenWidth(BuildContext context) {
    return MediaQuery.of(context).size.width < 350 && MediaQuery.of(context).size.height < 600;
  }

  static bool isMediumScreenWidth(BuildContext context) {
    return MediaQuery.of(context).size.width < 450 && MediaQuery.of(context).size.height < 800;
  }

  static bool isLargeScreenWidth(BuildContext context) {
    return MediaQuery.of(context).size.width > 400 && MediaQuery.of(context).size.height > 800;
  }
}
