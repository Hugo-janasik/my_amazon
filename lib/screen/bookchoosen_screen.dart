import 'package:flutter/material.dart';
import 'package:mywebsite/model/book_model.dart';
import 'package:mywebsite/model/commentary_model.dart';
import 'package:mywebsite/utils/readJson.dart';
import 'package:mywebsite/utils/screen.dart';
import 'package:mywebsite/widgets/cards/classic_card.dart';
import 'package:mywebsite/widgets/divider/my_divider.dart';
import 'package:mywebsite/widgets/image/image_networt.dart';
import 'package:mywebsite/widgets/rating_stars.dart/rating_stars.dart';
import 'dart:math';


class BookChoosenScreen extends StatefulWidget {
  final Book book;
  final List<Book> bookBought; 

  const BookChoosenScreen({Key? key, required this.book, required this.bookBought}) : super(key: key);

  @override
  _BookChoosenScreenState createState() => _BookChoosenScreenState();
}

class _BookChoosenScreenState extends State<BookChoosenScreen> {
  Random random = Random();

  String _multipleSynopsis(List<String> synopsis) => synopsis.toString().replaceAll('[', '').replaceAll(']', '');

  Widget _imageBook() => Padding(
    padding: const EdgeInsets.all(50),
    child: Container(
      height: Screen.fullHeigthScreen(context) / 1.5,
      width: Screen.fullWidthScreen(context) / 3,
      color: Colors.transparent,
      child: ImageNetwork(urlCover: widget.book.urlCover, color: Colors.transparent),
    ),
  );

  Widget _starsBook() => const RatingStars();

  Widget _buttonAddCart() {
        return ElevatedButton(
          onPressed: () => widget.bookBought.add(widget.book),
          style: ButtonStyle(backgroundColor: MaterialStateProperty.all(Colors.orange)),
          child: Row(
            children: [
              const Icon(Icons.shopping_cart),
              const Text('Ajouter au panier'),
              Padding(
                padding: const EdgeInsets.only(left: 30),
                child:  Text(
                  "${widget.book.price.toString()} €",
                   style: const TextStyle(fontWeight: FontWeight.bold, fontSize: 18),
                ),
              ),
            ],
          )
        );
  }

  Widget _titleSynopsisBook() => Wrap(
    children: [
      Padding(
        padding: const EdgeInsets.all(60),
        child: Column(
          mainAxisAlignment: MainAxisAlignment.start,
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            SizedBox(
              child: Text(widget.book.title, style: const TextStyle(fontWeight: FontWeight.bold, fontSize: 20))
            ),
            _starsBook(),
            Padding(
              padding: const EdgeInsets.only(top: 40),
              child: SizedBox(
                width: Screen.fullWidthScreen(context) / 2.2,
                height: Screen.fullHeigthScreen(context) / 2,
                child: Text(
                  _multipleSynopsis(widget.book.synopsis),
                  overflow: TextOverflow.ellipsis,
                  maxLines: 100,
                ),
              ),
            ),
            _buttonAddCart(),
          ],
        ),
      ),
    ],
  );

  Widget _commentariesDisplay()  => FutureBuilder(
    future: readJsonDataCommentary(),
    builder: (context, data) {
      if (data.hasError) {
          return Center(child: Text("${data.error}"));
        } else if (data.hasData) {
          var items = data.data as List<Commentary>;
          List<MyCard> cards = [];
          for (var i in items) {
            cards.add(MyCard(title: i.name, subtitle: Text(i.commentary)));
          }
          return Column(children: cards,);
        } else {
          return const Center( child: CircularProgressIndicator() );
        }
    }
  );

  Widget _displayBookChoosen() {
    return Wrap(children: [
      Column(
        mainAxisAlignment: MainAxisAlignment.start,
        mainAxisSize: MainAxisSize.max,
        children: [
          Row(
            mainAxisAlignment: MainAxisAlignment.start,
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              _imageBook(),
              _titleSynopsisBook(),
            ],
          ),
          const MyDivider(),
          const Text('Commentaires', style: TextStyle(fontWeight: FontWeight.bold, fontSize: 30),),
          Container(height: 30,),
          _commentariesDisplay(),
        ],
      )
    ]);
  }

  @override
  Widget build(BuildContext context) {
        return Scaffold(
          appBar: AppBar(
            leading: IconButton(
          icon: const Icon(Icons.arrow_back_ios),
          onPressed: () {
            Navigator.pop(context, widget.bookBought);
          },
        ),
           title: Text(widget.book.title),
          ),
          body: SingleChildScrollView(child: _displayBookChoosen()), 
        );
      }
}