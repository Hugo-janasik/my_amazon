import 'package:flutter/material.dart';
import 'package:mywebsite/model/book_model.dart';
import 'package:mywebsite/model/offers_model.dart';
import 'package:mywebsite/providers/offers_provider.dart';
import 'package:mywebsite/widgets/cards/classic_card.dart';
import 'package:mywebsite/widgets/divider/my_divider.dart';
import 'package:mywebsite/widgets/image/image_networt.dart';
import 'package:provider/provider.dart';


class MyShoppingCartScreen extends StatefulWidget {
  final List<Book>? books;
  
  const MyShoppingCartScreen({Key? key, this.books}) : super(key: key);

  @override
  _MyShoppingCartScreenState createState() => _MyShoppingCartScreenState();
}

class _MyShoppingCartScreenState extends State<MyShoppingCartScreen> {

  late List<String> tmpStr;
  List<Offers> offers = [];

  Widget _displayCardBookBought() {
    if (widget.books!.isEmpty) {
      return Container(
        padding: const EdgeInsets.only(top: 150),
        child: const Text(
          'Panier vide',
          textAlign: TextAlign.center,
          style: TextStyle(fontWeight: FontWeight.bold, fontSize: 15),
        ),
      );
    }
    return ListView.builder(
      shrinkWrap: true,
      itemCount: widget.books!.length,
      itemBuilder: (BuildContext context, index) {
        return  MyCard(
          title: widget.books![index].title,
          subtitle: const Text('Folio Junior'),
          leading: ImageNetwork(urlCover: widget.books![index].urlCover,),
          trailing: Text('${widget.books![index].price.toString()} €'),
        );
      }
    );
  }

  double _setAmount() {
    double sumTotal = 0;

    for (var element in widget.books!) {
      sumTotal += element.price;
    }

    double sumOffers = sumTotal;

    for (var element in offers) { 
      switch (element.type) {
        case TypeOffers.MINUS:
          if (sumOffers > (sumTotal - element.value)) {
            sumOffers = sumTotal - element.value;
          }
        break;
        case TypeOffers.PERCENTAGE:
          if (sumOffers > (sumTotal - sumTotal * (element.value / 100))) {
            sumOffers = sumTotal - sumTotal * (element.value / 100);
          }
        break;
        case TypeOffers.SLICE:
          if (element.sliceValue! <= sumTotal) {
            if (sumOffers > sumTotal - element.value) {
              double multiplicator = sumTotal / element.sliceValue!;
              sumOffers = sumTotal - (element.value * multiplicator);
            }
          }
        break;
        case TypeOffers.NULL:
        break;
      }
    }
    return sumOffers;
  }

  Widget _displaySum() => Align(
    child: Column(
      children: [
        const MyDivider(),
        Text(
          'Prix à payer avec une réduction : ${_setAmount().toString()} €',
          style: const TextStyle(fontWeight: FontWeight.bold, fontSize: 20),
        ),
      ],
    ),
    alignment: Alignment.bottomCenter,
  );

  @override
    void initState() {
      super.initState();
      if (widget.books!.isEmpty) {
        tmpStr = List.empty();
        return;
      }
      tmpStr = [];
      for (var element in widget.books!) { 
        tmpStr.add(element.isbn);
      }
    }

  @override
  Widget build(BuildContext context) {
    final OffersProvider provider = Provider.of<OffersProvider>(context, listen: true);

    return Scaffold(
      appBar: AppBar(
        title: const Text('Mes Achats'),
      ),
      body: FutureBuilder<List<Offers>>(
        future: provider.offersFromApi(tmpStr),
        builder: (context, snapshot) {
          if (snapshot.hasError) {
            return const Center(child: Text('An error has occured'));
          } else if (snapshot.hasData) {
            offers = snapshot.data!;
            return SingleChildScrollView(child: Column(
              mainAxisAlignment: MainAxisAlignment.center,
              crossAxisAlignment: CrossAxisAlignment.center,
              children: [
              _displayCardBookBought(),
              widget.books!.isNotEmpty
                ? _displaySum() 
                : Container(),
              ],
            ),); 
          } else {
            return const Center(child: CircularProgressIndicator());
          }
        }
      ),
    );
  }
}