import 'package:flutter/material.dart';
import 'package:mywebsite/model/book_model.dart';
import 'package:mywebsite/providers/book_provider.dart';
import 'package:mywebsite/screen/bookchoosen_screen.dart';
import 'package:mywebsite/screen/shoppingCart_screen.dart';
import 'package:mywebsite/widgets/image/image_networt.dart';
import 'package:mywebsite/widgets/rating_stars.dart/rating_stars.dart';
import 'package:mywebsite/widgets/search_bar/search_bar.dart';
import 'package:provider/provider.dart';

class BookScreen extends StatefulWidget {

  const BookScreen({Key? key}) : super(key: key);

  @override
  _BookScreenState createState() => _BookScreenState();
}

class _BookScreenState extends State<BookScreen> {

  String _searchText = '';
  List<Book> bookListBought = [];

  List<Widget> _listTileBooks(List<Book> books, RegExp regexp) {
    final List<Widget> _listTilesBooks = [];
    int index = 0;

    _listTilesBooks.add(
      Padding(
        padding: const EdgeInsets.symmetric(vertical: 50, horizontal: 200),
        child:  SearchBar(
          context,
          onChanged: (String value) =>
            setState(() => _searchText = value),
          hintText: 'Rechercher',
        )
      )
    );

    for (var book in books) {
      if (index >= 4) {
        _listTilesBooks.add(Row(mainAxisAlignment: MainAxisAlignment.start,));
        _listTilesBooks.add(const SizedBox(height: 70,));
        index = 0;
      }
      if (regexp.hasMatch(book.title)) {
         _listTilesBooks.add(
        InkWell(
          onTap: (() async {
            await Navigator.push(context, MaterialPageRoute(
              builder: (BuildContext context) => BookChoosenScreen(
                book: book,
                bookBought: bookListBought,
              ),
            ),);
            setState(() { bookListBought; });
          }),
          child: Column(
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              ImageNetwork(urlCover: book.urlCover),
              Text(book.title, style: const TextStyle(fontSize: 13,), textAlign: TextAlign.left),
              const RatingStars(),
              const Text('Folio Junior', style: TextStyle(color: Colors.blueGrey)),
              Text(
                "${book.price.toString()} €",
                style: const TextStyle(fontWeight: FontWeight.bold, fontSize: 18),
              ),
            ],
          )
        )
      );
       _listTilesBooks.add(const SizedBox(width: 50));
      index++;
      }
    }
    return _listTilesBooks;
  }

  Widget _displayBooks(List<Book> books) {
    return Center(
       child: Wrap(children: _listTileBooks(books, RegExp(_searchText)), alignment: WrapAlignment.center,),
    );
  }

  @override
  Widget build(BuildContext context) {
    final BookProvider provider = Provider.of<BookProvider>(context, listen: false);

    return Scaffold(
      appBar: AppBar(
        actions: [
          Padding(
            padding: const EdgeInsets.only(right: 20),
            child: SizedBox(
              child: Stack(
                children: [
                  IconButton(
                    onPressed: () {
                      Navigator.push(context, MaterialPageRoute(
                      builder: (BuildContext context) => MyShoppingCartScreen(books: bookListBought),
                    ));
                    },
                    icon: const Icon(Icons.shopping_cart)
                  ),
                  Text(bookListBought.length.toString()),
                ],
              ),
            )
          ),
        ],
        title: const Text('My Amazon'),
      ),
      body: FutureBuilder<List<Book>>(
        future: provider.bookFromAPi(),
        builder: (context, snapshot) {
          if (snapshot.hasError) {
            return const Center(child: Text('An error has occured'));
          } else if (snapshot.hasData) {
            return SingleChildScrollView(child: _displayBooks(snapshot.data!)); 
          } else {
            return const Center(child: CircularProgressIndicator());
          }
        }
      ),
    );
  }
}