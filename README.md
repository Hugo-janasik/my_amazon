
# My Amazon

### Prerequisites

Requirements for the software and other tools to build, test and push 
- [Flutter](https://docs.flutter.dev/get-started/install)
- [Chrome](https://chromeenterprise.google/browser/download/?utm_source=adwords&utm_medium=cpc&utm_campaign=2022-H1-chromebrowser-paidmed-paiddisplay-other-chromebrowserent&utm_term=downloadnow-chrome-browser-download&utm_content=GCEJ&brand=GCEJ&gclid=CjwKCAjwwdWVBhA4EiwAjcYJEK59fBdibBsXy7NPkNHAVE3dTSbry35bMiEgtCWGTpDPzVvvwOrBQxoCCB4QAvD_BwE&gclsrc=aw.ds#windows-tab)
- [VScode](https://code.visualstudio.com/download)

### Installing

In order to run the project you have to follow these commands:

    $ git clone
    $ cd my_amazon/
    $ flutter pub get 
    $ flutter run ./lib/main/main_dev.dart -d chrome --web-renderer html  

